package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("This is a sample application")
	handler := &Handler{}
	err := http.ListenAndServe(":8123", handler)
	if err != nil {
		fmt.Println(err)
	}
}

type Handler struct {
}

func (this *Handler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	rw.Write([]byte("OK"))
}
